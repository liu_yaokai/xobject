//
// Created by "Yaokai Liu" on 12/16/22.
//

#ifndef X_XOBJECT_H
#define X_XOBJECT_H

#include "xtypes.h"
#include "xdef.h"

typedef enum {
    link_execute  = 1,
    link_static   = 2,
    link_dynamic  = 3,
} l_form;

typedef enum {
    executable  = 1,
    read_only   = 2,
    read_write  = 3,
} var_kind;


/*
 * xObjFile layout
 *  +-----------------------------------+    ---+
 *  | version: 64 bit unsigned long     |       |
 *  +-----------------------------------+       |
 *  | arch_num: 64 bit unsigned long    |       |
 *  +-----------------------------------+       |
 *  | os_num: 64 bit unsigned long      |       |
 *  +----------------+------------------+       +--- Metadata (48 byte)
 *  | form: 2 bit ub | entry: 62 bit ul |       |
 *  +----------------+------------------+       |
 *  | length: 64 bit unsigned long      |       |
 *  +-----------------------------------+       |
 *  | n_fields: 64 bit unsigned long    |       |
 *  +-----------------------------------+    ---+
 *  | table[0]: t_item (32 byte)        |       |
 *  +-----------------------------------+       |
 *   ...       ...                              |
 *                                              |
 *  +-----------------------------------+       |
 *  | table[k]: t_item (32 byte)        |       +--- Field Table (32x byte)
 *  +-----------------------------------+       |
 *   ...       ...                              |
 *                                              |
 *  +-----------------------------------+       |
 *  | table[n_fields-1]:t_item (32 byte)|      |
 *  +-----------------------------------+    ---+
 *
 *  total header size: 48 + 32x byte
 *
 *  Specially:
 *      length = 48 + 32 * n_fields + sum(1, n_fields, lambda #: table[#].length)
 */
typedef struct xObjFileHeader xObjFile;


/*
 * xField layout
 *  +-----------------------------------+    ---+
 *  | length: 64 bit unsigned long      |       |
 *  +-----------------------------------+       +--- Field Metadata (16 byte)
 *  | n_vars: 64 bit unsigned long      |       |
 *  +-----------------------------------+    ---+
 *  | table[0]: t_item (32 byte)        |       |
 *  +-----------------------------------+       |
 *   ...       ...                              |
 *                                              |
 *  +-----------------------------------+       |
 *  | table[k]: t_item (32 byte)        |       +--- Symbol Table (32x byte)
 *  +-----------------------------------+       |
 *   ...       ...                              |
 *                                              |
 *  +-----------------------------------+       |
 *  | table[n_vars-1]: t_item (32 byte) |       |
 *  +-----------------------------------+    ---+
 *
 *  every object header size: 16 + 32x byte
 *
 *  Specially:
 *      length = 16 + 32 * n_vars + sum(1, n_vars, lambda #: table[#].length)
 */
typedef struct xObjHeader xObj;


/*
 * xVariable layout
 *  +---------------------------------------+    ---+
 *  | kind: 4 bit enum | length: 60 bit ul  |       |
 *  +---------------------------------------+       +--- Variable Metadata (16 byte)
 *  | entry: 64 bit long                    |       |
 *  +---------------------------------------+    ---+
 *  | data[]: n ubyte (aligned 16 byte)     |       |
 *  |                                       |       |
 *   ...       ...                                  +--- Data (16x byte)
 *                                                  |
 *  |                                       |       |
 *  +---------------------------------------+    ---+
 *
 *  every variable size: 16 + 16x byte
 *
 *  Specially:
 *      length = 16 + 16x
 */
typedef struct xVariable xVariable;

void xObject_resolveObjFile(void * _buffer, xuInt * _file_handle);
void xObject_makeObjFile(xVariable * _variable, void * _buffer);


#endif //X_XOBJECT_H
