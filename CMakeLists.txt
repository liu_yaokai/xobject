cmake_minimum_required(VERSION 3.24)
project(xObject C)

set(CMAKE_C_STANDARD 23)
add_compile_options("-fno-builtin")
add_compile_definitions("XTYPES_USING_STDC_TYPE")

add_library(xObject
        xobject.h xobject.c)