//
// Created by Yaokai Liu on 12/17/22.
//

#include "xobject.h"

#define     XOBJECT_MAX_ID_LEN  24

// table item
typedef struct {
    xuByte      ident[XOBJECT_MAX_ID_LEN];
    xPtrDiff    offset;
} t_item;

typedef struct xObjFileHeader {
    struct {
        xuLong  version;
        xuLong  arch_num;
        xuLong  os_num;
        l_form  form: 2;
        xuLong  entry: bitsof(xuLong) - 2;
    }       link_info;
    xuLong  length;
    xuLong  n_fields;
    t_item  table[];
} xObjFileHeader;


typedef struct xObjHeader {
    xuLong  length;
    xuLong  n_vars;
    t_item  table[];
} xFieldHeader;


typedef struct xVariable {
    var_kind    kind: 4;
    xuLong      length: bitsof(xuLong) - 4;
    xPtrDiff    entry;
    xuByte      data[] __attribute__((aligned(16)));
} xVariable;

